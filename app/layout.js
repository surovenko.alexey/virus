import {Inter} from "next/font/google";
import "./globals.css";

const inter = Inter({subsets: ["latin"]});

export const viewport = {
    width: 'device-width',
    initialScale: 1,
    maximumScale: 1,
    userScalable: false,
}

export const metadata = {
    title: "Surovenko",
    description: "Surovenko",
    icons: {
        icon: [
            {
                media: '(prefers-color-scheme: dark)',
                url: '/images/favicon.ico',
                href: '/images/favicon.ico'
            },
            {
                media: '(prefers-color-scheme: light)',
                url: '/images/favicon.ico',
                href: '/images/favicon.ico'
            }
        ]
    }
};

export default function RootLayout({children}) {
    return (
        <html lang="en">
        <body className={inter.className}>{children}</body>
        </html>
    );
}
