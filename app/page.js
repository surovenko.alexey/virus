'use client'

import Image from "next/image";
import {gradient, service2, service3} from "@/public/assets";
import Link from "next/link";

export default function Page() {
    return (
        <>
            <div
                className="hidden absolute top-0 left-5 w-0.25 h-full bg-stroke-1 pointer-events-none md:block lg:left-7.5 xl:left-10"/>
            <div
                className="hidden absolute top-0 right-5 w-0.25 h-full bg-stroke-1 pointer-events-none md:block lg:right-7.5 xl:right-10"/>
            <div className='py-10 lg:py-16 xl:py-20'>
                <div className="container">
                    <div
                        className="absolute top-0 -left-[10rem] w-[56.625rem] h-[56.625rem] opacity-50 mix-blend-color-dodge pointer-events-none">
                        <Image
                            className="absolute top-1/2 left-1/2 w-[64.5625rem] max-w-[79.5625rem] h-[58.5625rem] -translate-x-1/2 -translate-y-1/2"
                            src={gradient}
                            width={600}
                            height={600}
                            alt="Gradient"
                        />
                    </div>
                    <div className="relative">

                        <div className="relative z-1 grid gap-5 lg:grid-cols-2">
                            <Link className='transition-transform duration-300 ease-in-out transform hover:scale-105' href='/aleksei'>
                            <div className="relative min-h-[39rem] border border-n-1/10 rounded-3xl overflow-hidden">
                                <div className="absolute inset-0">
                                    <Image
                                        src={service2}
                                        className="h-full w-full object-cover"
                                        width={630}
                                        height={750}
                                        alt="robot"
                                    />
                                </div>

                                <div
                                    className="absolute inset-0 flex flex-col justify-end p-8 bg-gradient-to-b from-n-8/0 to-n-8/90 lg:p-15">
                                    <h4 className="h4 mb-4">Aleksei</h4>
                                    <p className="body-2 mb-[3rem] text-n-3">
                                        Some short info about Aleksei? Whatever...
                                    </p>
                                </div>
                            </div>
                            </Link>

                            <Link className='transition-transform duration-300 ease-in-out transform hover:scale-105' href='/yana'>
                            <div className="relative min-h-[39rem] border border-n-1/10 rounded-3xl overflow-hidden">
                                <div className="absolute inset-0">
                                    <Image
                                        src={service3}
                                        className="h-full w-full object-cover"
                                        width={630}
                                        height={750}
                                        alt="robot"
                                    />
                                </div>

                                <div
                                    className="absolute inset-0 flex flex-col justify-end p-8 bg-gradient-to-b from-n-8/0 to-n-8/90 lg:p-15">
                                    <h4 className="h4 mb-4">Yana</h4>
                                    <p className="body-2 mb-[3rem] text-n-3">
                                        My wife. I made a choose once. Now I don't need it anymore
                                    </p>
                                </div>
                            </div>
                            </Link>
                        </div>

                    </div>
                </div>
            </div>
        </>
    )
}
