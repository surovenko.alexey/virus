'use client'

import Collaboration from "@/app/(template)/components/Collaboration";
import Benefits from "@/app/(template)/components/Benefits";
import Services from "@/app/(template)/components/Services";
import Roadmap from "@/app/(template)/components/Roadmap";
import Pricing from "@/app/(template)/components/Pricing";
import Footer from "@/app/(template)/components/Footer";
import Hero from "@/app/(template)/components/Hero";

export default function Page() {
    return (
        <>
            <div className="pt-[4.75rem] lg:pt-[5.25rem] overflow-hidden">
                {/*<Header />*/}
                <Hero/>
                <Benefits/>
                <Collaboration/>
                <Services/>
                {/*<Pricing/>*/}
                <Roadmap/>
                <Footer/>
            </div>
        </>
    )
}
