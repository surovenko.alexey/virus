export const dynamic = 'force-dynamic' // defaults to auto

function uuidv4() {
    return "10000000-1000-4000-8000-100000000000".replace(/[018]/g, c =>
        (+c ^ crypto.getRandomValues(new Uint8Array(1))[0] & 15 >> +c / 4).toString(16)
    );
}

const tables = [
    {
        id: '1',
        defaultHours: 2,
        maxHours: 6,
        persons: 8,
        left: 150,
        top: 155,
        w: 400,
        h: 275
    },
    {
        id: '2',
        defaultHours: 3,
        maxHours: 4,
        name: 'second table',
        persons: 8,
        left: 700,
        top: 155,
        w: 400,
        h: 275
    },
    {
        id: '3',
        maxHours: 4,
        persons: 8,
        left: 1690,
        top: 155,
        w: 400,
        h: 275
    },
    {
        id: '4',
        maxHours: 4,
        persons: 8,
        left: 2250,
        top: 155,
        w: 400,
        h: 275
    },
    {
        id: '5',
        maxHours: 4,
        persons: 6,
        left: 57,
        top: 590,
        w: 450,
        h: 290,
        chip: {
            left: -10,
            top: 1
        },
    },
    {
        id: '6',
        maxHours: 4,
        persons: 6,
        left: 57,
        top: 910,
        w: 450,
        h: 290,
        chip: {
            left: -10,
            top: 1
        },
    },
    {
        id: '7',
        maxHours: 4,
        persons: 6,
        left: 57,
        top: 1225,
        w: 450,
        h: 290,
        chip: {
            left: -10,
            top: 1
        },
    },
    {
        id: '8',
        maxHours: 2,
        persons: 4,
        left: 730,
        top: 605,
        w: 220,
        h: 220,
        chip: {
            left: -1,
            top: -2
        },
    },
    {
        id: '9',
        maxHours: 2,
        persons: 4,
        left: 730,
        top: 900,
        w: 220,
        h: 220,
        chip: {
            left: -1,
            top: -2
        },
    },
    {
        id: '10',
        maxHours: 2,
        persons: 4,
        left: 730,
        top: 1195,
        w: 220,
        h: 220,
        chip: {
            left: -1,
            top: -2
        },
    },
    {
        id: '11',
        maxHours: 2,
        persons: 4,
        left: 1020,
        top: 605,
        w: 220,
        h: 220,
        chip: {
            left: -1,
            top: -2
        },
    },
    {
        id: '12',
        maxHours: 2,
        persons: 4,
        left: 1020,
        top: 900,
        w: 220,
        h: 220,
        chip: {
            left: -1,
            top: -2
        },
    },
    {
        id: '13',
        maxHours: 2,
        persons: 4,
        left: 1020,
        top: 1195,
        w: 220,
        h: 220,
        chip: {
            left: -1,
            top: -2
        },
    },
    {
        id: '14',
        maxHours: 2,
        persons: 4,
        left: 1610,
        top: 605,
        w: 220,
        h: 220,
        chip: {
            left: -1,
            top: -2
        },
    },
    {
        id: '15',
        maxHours: 2,
        persons: 4,
        left: 1610,
        top: 900,
        w: 220,
        h: 220,
        chip: {
            left: -1,
            top: -2
        },
    },
    {
        id: '16',
        maxHours: 2,
        persons: 4,
        left: 1610,
        top: 1195,
        w: 220,
        h: 220,
        chip: {
            left: -1,
            top: -2
        },
    },
    {
        id: '17',
        maxHours: 2,
        persons: 4,
        left: 1890,
        top: 605,
        w: 220,
        h: 220,
        chip: {
            left: -1,
            top: -2
        },
    },
    {
        id: '18',
        maxHours: 2,
        persons: 4,
        left: 1890,
        top: 900,
        w: 220,
        h: 220,
        chip: {
            left: -1,
            top: -2
        },
    },
    {
        id: '19',
        maxHours: 2,
        persons: 4,
        left: 1890,
        top: 1195,
        w: 220,
        h: 220,
        chip: {
            left: -1,
            top: -2
        },
    },
    {
        id: '20',
        maxHours: 2,
        persons: 2,
        left: 2270,
        top: 635,
        w: 380,
        h: 115,
    },
    {
        id: '21',
        maxHours: 2,
        persons: 2,
        left: 2270,
        top: 843,
        w: 380,
        h: 115,
    },
    {
        id: '22',
        maxHours: 2,
        persons: 2,
        left: 2270,
        top: 1056,
        w: 380,
        h: 115
    },
    {
        id: '23',
        maxHours: 2,
        persons: 2,
        left: 2270,
        top: 1265,
        w: 380,
        h: 115
    },
]

const cfg = {
    verticalMultiplier: 1.4,

    dates: {
        from: 0,
        to: 14,
        days: [1, 2, 3, 4, 5, 6],
        ranges: [
            {
                from: 10,
                to: 11
            }, {
                from: 15,
                to: 23
            }
        ]
    },

    tables: tables,

    data: [],
}

export async function GET(request) {
    return Response.json(cfg)
}

export async function POST(request) {
    const body = await request.json();

    const res = {
        id: uuidv4(),
        table: body.table,
        from: body.date,
        to: body.date + (body.hours * 3600000)
    }
    cfg.data.push(res);
    return Response.json(cfg);
}