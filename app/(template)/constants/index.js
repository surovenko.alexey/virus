import {
  benefitIcon1,
  benefitIcon2,
  benefitIcon3,
  benefitIcon4,
  benefitImage2,
  chromecast,
  disc02,
  discord,
  discordBlack,
  facebook,
  figma,
  file02,
  framer,
  homeSmile,
  instagram,
  lineate,
  lineate_logo,
  deeplay,
  notification2,
  notification3,
  notification4,
  notion,
  photoshop,
  plusSquare,
  protopie,
  raindrop,
  recording01,
  recording03,
  roadmap1,
  roadmap2,
  roadmap3,
  roadmap4,
  searchMd,
  slack,
  sliders04,
  telegram,
  twitter,
  yourlogo,
  zeta,
  zeta_logo,
  educamp
} from "../../../public/assets";

export const navigation = [
  {
    id: "0",
    title: "Features",
    url: "#features",
  },
  {
    id: "1",
    title: "Pricing",
    url: "#pricing",
  },
  {
    id: "2",
    title: "How to use",
    url: "#how-to-use",
  },
  {
    id: "3",
    title: "Roadmap",
    url: "#roadmap",
  },
  {
    id: "4",
    title: "New account",
    url: "#signup",
    onlyMobile: true,
  },
  {
    id: "5",
    title: "Sign in",
    url: "#login",
    onlyMobile: true,
  },
];

export const heroIcons = [homeSmile, file02, searchMd, plusSquare];

export const notificationImages = [notification4, notification3, notification2];

export const companyLogos = [zeta, lineate, deeplay];

export const brainwaveServices = [
  "Hard skills",
  "Soft skills",
  "Leadership attributes",
];

export const brainwaveServicesIcons = [
  recording03,
  recording01,
  disc02,
  chromecast,
  sliders04,
];

export const roadmap = [
  {
    id: "0",
    title: "Any good thing about me",
    text: "Just good at...",
    date: "May 2023",
    status: "done",
    imageUrl: roadmap1,
    colorful: true,
  },
  {
    id: "1",
    title: "Almost there...",
    text: "Almost perfect full-stack craftsmen in IT field. Just few more thing and the last uncovered position will be here",
    date: "September 2024",
    status: "progress",
    imageUrl: roadmap2,
  },
  {
    id: "2",
    title: "Leading performance specialist at the company",
    text: "I was appointed as the leading performance specialist in the company early in my career, celebrated for my outstanding work in high-performance server optimization and hardware tuning",
    date: "May 2019",
    status: "done",
    imageUrl: roadmap3,
  },
  {
    id: "3",
    title: "Any good thing about me",
    text: "Just good at...",
    date: "May 2023",
    status: "progress",
    imageUrl: roadmap4,
  },
];

export const collabText =
  "Allow me to introduce you to a few of the key areas where I have developed strong expertise and demonstrated significant proficiency throughout my career";

export const collabContent = [
  {
    id: "0",
    title: "Full-stack Craftsmen",
    text: "Backend, Frontend, QA, DevOps, and Tech Lead—all areas covered with hands-on experience",
  },
  {
    id: "1",
    title: "The one who can drive a project from inception to delivery",
    text: "Regardless of the challenge or field, top skills are at hand",
  },
];

export const collabApps = [
  {
    id: "0",
    title: "Figma",
    icon: figma,
    width: 26,
    height: 36,
  },
  {
    id: "1",
    title: "Notion",
    icon: notion,
    width: 34,
    height: 36,
  },
  {
    id: "2",
    title: "Discord",
    icon: discord,
    width: 36,
    height: 28,
  },
  {
    id: "3",
    title: "Slack",
    icon: slack,
    width: 34,
    height: 35,
  },
  {
    id: "4",
    title: "Photoshop",
    icon: photoshop,
    width: 34,
    height: 34,
  },
  {
    id: "5",
    title: "Protopie",
    icon: protopie,
    width: 34,
    height: 34,
  },
  {
    id: "6",
    title: "Framer",
    icon: framer,
    width: 26,
    height: 34,
  },
  {
    id: "7",
    title: "Raindrop",
    icon: raindrop,
    width: 38,
    height: 32,
  },
];

export const pricing = [
  {
    id: "0",
    title: "Basic",
    description: "AI chatbot, personalized recommendations",
    price: "0",
    features: [
      "An AI chatbot that can understand your queries",
      "Personalized recommendations based on your preferences",
      "Ability to explore the app and its features without any cost",
    ],
  },
  {
    id: "1",
    title: "Premium",
    description: "Advanced AI chatbot, priority support, analytics dashboard",
    price: "9.99",
    features: [
      "An advanced AI chatbot that can understand complex queries",
      "An analytics dashboard to track your conversations",
      "Priority support to solve issues quickly",
    ],
  },
  {
    id: "2",
    title: "Enterprise",
    description: "Custom AI chatbot, advanced analytics, dedicated account",
    price: null,
    features: [
      "An AI chatbot that can understand your queries",
      "Personalized recommendations based on your preferences",
      "Ability to explore the app and its features without any cost",
    ],
  },
];

export const benefits = [
  {
    id: "0",
    title: "Zeta Global player",
    text: "Currently working with Zeta Global on OpenRTB solution with high performance services",
    backgroundUrl: "../assets/benefits/card-1.svg",
    iconUrl: zeta_logo,
    imageUrl: zeta_logo,
  },
  {
    id: "1",
    title: "Educamp co-founder",
    text: "Co-founder of Educamp, a platform designed for creating, uploading, and managing learning courses",
    backgroundUrl: "../assets/benefits/card-2.svg",
    iconUrl: educamp,
    imageUrl: educamp,
    light: true,
  },
  {
    id: "2",
    title: "Lineate work experience",
    text: "I bring a wealth of experience from my time at Lineate, where I developed into a highly skilled specialist",
    backgroundUrl: "../assets/benefits/card-3.svg",
    iconUrl: lineate_logo,
    imageUrl: lineate_logo,
  },
  {
    id: "3",
    title: "Different solutions with Deeplay",
    text: "Even have the experience working with huge systems on Windows, with money based workflow",
    backgroundUrl: "../assets/benefits/card-4.svg",
    iconUrl: deeplay,
    imageUrl: deeplay,
    light: true,
  },
  {
    id: "4",
    title: "Ask anything",
    text: "Lets users quickly find answers to their questions without having to search through multiple sources.",
    backgroundUrl: "../assets/benefits/card-5.svg",
    iconUrl: benefitIcon1,
    imageUrl: benefitImage2,
  },
  {
    id: "5",
    title: "Improve everyday",
    text: "The app uses natural language processing to understand user queries and provide accurate and relevant responses.",
    backgroundUrl: "../assets/benefits/card-6.svg",
    iconUrl: benefitIcon2,
    imageUrl: benefitImage2,
  },
];

export const socials = [
  {
    id: "0",
    title: "Discord",
    iconUrl: discordBlack,
    url: "#",
  },
  {
    id: "1",
    title: "Twitter",
    iconUrl: twitter,
    url: "#",
  },
  {
    id: "2",
    title: "Instagram",
    iconUrl: instagram,
    url: "#",
  },
  {
    id: "3",
    title: "Telegram",
    iconUrl: telegram,
    url: "#",
  },
  {
    id: "4",
    title: "Facebook",
    iconUrl: facebook,
    url: "#",
  },
];
